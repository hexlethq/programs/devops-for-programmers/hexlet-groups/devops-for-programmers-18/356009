terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  type = string
}

variable "do_ssh_key_name" {
  type = string
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "primary" {
  name = var.do_ssh_key_name
}

resource "digitalocean_vpc" "cc_net" {
  name   = "web-security-network-cc-net"
  region = "ams3"
}

resource "digitalocean_firewall" "bastion" {
  name = "web-security-network-bastion-firewall"

  droplet_ids = [digitalocean_droplet.bastion.id]

  inbound_rule {
    protocol = "icmp"
  }

  inbound_rule {
    protocol = "tcp"
    port_range = "22"
  }

  outbound_rule {
    protocol = "icmp"
  }
}

locals {
  secure_droplets = concat(
    [for droplet in digitalocean_droplet.droplets : droplet.id],
    [digitalocean_droplet.bastion.id]
  )
}

resource "digitalocean_firewall" "droplets" {
  name = "web-security-network-droplet-firewall"

  droplet_tags = ["web-security-homework"]

  inbound_rule {
    protocol = "tcp"
    source_droplet_ids = local.secure_droplets
    source_load_balancer_uids = [digitalocean_loadbalancer.loadbalancer]
  }

  inbound_rule {
    protocol = "udp"
    source_droplet_ids = local.secure_droplets
    source_load_balancer_uids = [digitalocean_loadbalancer.loadbalancer]
  }

  inbound_rule {
    protocol = "icmp"
    source_droplet_ids = local.secure_droplets
    source_load_balancer_uids = [digitalocean_loadbalancer.loadbalancer]
  }

  outbound_rule {
    protocol = "tcp"
    source_droplet_ids = local.secure_droplets
    destination_load_balancer_uids = [digitalocean_loadbalancer.loadbalancer]
  }

  outbound_rule {
    protocol = "udp"
    source_droplet_ids = local.secure_droplets
    destination_load_balancer_uids = [digitalocean_loadbalancer.loadbalancer]
  }

  outbound_rule {
    protocol = "icmp"
    source_droplet_ids = local.secure_droplets
    destination_load_balancer_uids = [digitalocean_loadbalancer.loadbalancer]
  }
}

resource "digitalocean_droplet" "droplets" {
  count = 2
  image  = "docker-20-04"
  name   = "web-security-homework-${count.index}"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.primary.id]
  tags = ["web-security-homework"]
  vpc_uuid = digitalocean_vpc.cc_net.id
}

resource "digitalocean_droplet" "bastion" {
  image  = "docker-20-04"
  name   = "web-security-homework-bastion"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.primary.id]
  tags = ["web-security-homework-bastion"]
  vpc_uuid = digitalocean_vpc.cc_net.id
}

resource "digitalocean_loadbalancer" "loadbalancer" {
  name = "web-balancer-security"
  region = "ams3"

  droplet_tag = "web-security-homework"

  vpc_uuid = digitalocean_vpc.cc_net.id

  forwarding_rule {
    entry_port = "80"
    entry_protocol = "http"

    target_port = "5000"
    target_protocol = "http"
  }

  healthcheck {
    protocol = "http"
    path = "/"
    port = "5000"
  }
}

resource "digitalocean_domain" "afs_detailer_space" {
  name = "security.detailer.space"
  ip_address = digitalocean_loadbalancer.web.ip
}

output "private_droplet_ips" {
  value = [
    for droplet in digitalocean_droplet.droplets : droplet.ipv4_address_private
  ]
}
