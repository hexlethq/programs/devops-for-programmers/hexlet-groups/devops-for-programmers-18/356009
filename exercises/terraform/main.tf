terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  type = string
}

variable "do_ssh_key_name" {
  type = string
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "primary" {
  name = var.do_ssh_key_name
}

resource "digitalocean_droplet" "droplet_1" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-1"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.primary.id]
  tags = ["web-terraform-homework"]
}

resource "digitalocean_droplet" "droplet_2" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-2"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.primary.id]
  tags = ["web-terraform-homework"]
}

resource "digitalocean_loadbalancer" "web" {
  name = "web-balancer"
  region = "ams3"

  droplet_tag = "web-terraform-homework"

  forwarding_rule {
    entry_port = "80"
    entry_protocol = "http"

    target_port = "5000"
    target_protocol = "http"
  }

  healthcheck {
    protocol = "http"
    path = "/"
    port = "5000"
  }
}

resource "digitalocean_domain" "tform_detailer_space" {
  name = "tform.detailer.space"
  ip_address = digitalocean_loadbalancer.web.ip
}

output "created_droplet_ips" {
  value = [
    digitalocean_droplet.droplet_1.ipv4_address,
    digitalocean_droplet.droplet_2.ipv4_address
  ]
}
