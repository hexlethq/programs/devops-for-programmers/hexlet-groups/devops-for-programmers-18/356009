terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  type = string
}

variable "do_ssh_key_name" {
  type = string
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "primary" {
  name = var.do_ssh_key_name
}

resource "digitalocean_droplet" "droplets" {
  count = 2
  image  = "ubuntu-20-10-x64"
  name   = "web-ansible-for-servers-homework-${count.index}"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.primary.id]
  tags = ["web-ansible-for-servers-homework"]
}

resource "digitalocean_loadbalancer" "web" {
  name = "web-balancer-ansible-for-servers"
  region = "ams3"

  droplet_tag = "web-ansible-for-servers-homework"

  forwarding_rule {
    entry_port = "80"
    entry_protocol = "http"

    target_port = "80"
    target_protocol = "http"
  }

  healthcheck {
    protocol = "http"
    path = "/"
    port = "80"
  }
}

resource "digitalocean_domain" "afs_detailer_space" {
  name = "ansible-for-servers.detailer.space"
  ip_address = digitalocean_loadbalancer.web.ip
}

output "droplets" {
  value = [
    for droplet in digitalocean_droplet.droplets : droplet.ipv4_address
  ]
}

output "public_ssh_key" {
  value = data.digitalocean_ssh_key.primary
}
